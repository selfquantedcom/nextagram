import React from 'react'
import './App.css'
import { Route, Link, Switch } from 'react-router-dom'

const PricingPage = (props) => <h1>Pricing Page. Price is {props.price}</h1>
const AboutUsPage = () => <h1>About Us Page</h1>
const UserPage = props => (
  <div>
    <button
      onClick={() => {
        // purchase item logic here
        props.history.goBack()
      }}
    >
      Purchase Item
    </button>
    <button
      onClick={() => {
        props.history.push('/pricing')
      }}
    >
      Visit Pricing Page
    </button>
    <h1>User Page for id: {props.match.params.id}</h1>
  </div>
)

class App extends React.Component {
  render() {
    return (
      <>
        <h1>Learn React Router</h1>
        <Link className="m-2" to="/">
          Home
        </Link>
        <Link className="m-2" to="/users/about">
          Go to about
        </Link>
        <Link className="m-2" to="/pricing">
          Go to pricing
        </Link>

        <Link className="m-2" to="/users/1">
          User 1
        </Link>
        <Link className="m-2" to="/users/2">
          User 2
        </Link>
        <Link className="m-2" to="/users/3">
          User 3
        </Link>
        <Link className="m-2" to="/users/4">
          User 4
        </Link>

        <Switch>
          <Route path="/users/about" component={AboutUsPage} />
          <Route path="/users/:id" component={UserPage} />
          <Route path="/pricing" component={props => <PricingPage {...props} price={9.99} />} />
        </Switch>
      </>
    )
  }
}

export default App



<div>
        <Link to="/">Home</Link>        
        <Link to="/users/1">My Profile</Link>

        <Route exact path="/" component={props => {
          return <HomePage {...props} awaitingUsersResponse={this.state.awaitingUsersResponse} users={this.state.users}/>
        }} />
        <Route path="/users/:id" component={UserProfilePage} />
      </div>          

      props.getUserImages






      