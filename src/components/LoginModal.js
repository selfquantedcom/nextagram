import React from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import LoginForm from "./LoginForm"
import RegistrationForm from "./RegistrationForm"

class LoginModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      loginFormActive: null
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal,
      loginFormActive: true      
    }));
    this.props.userLogedIn()
  } 

  toggleForm = () => {
    (this.state.loginFormActive ? this.setState({loginFormActive: false}) : this.setState({loginFormActive: true}))
  }  

  render() {
    return (
      <div>
        <Button color="primary" onClick={this.toggle}>{this.props.buttonLabel}</Button>
        <Modal isOpen={this.state.modal} className={this.props.className}>                  
          {this.state.loginFormActive ? <ModalHeader toggle={this.toggle}>User Login</ModalHeader> : <ModalHeader toggle={this.toggle}>User Registration</ModalHeader>}
          <ModalBody> 
            {this.state.loginFormActive ? <LoginForm toggle={this.toggle} toggleForm={this.toggleForm}/> : <RegistrationForm toggleForm={this.toggleForm}/>}                       
          </ModalBody>                      
        </Modal>
      </div>
    );
  }
}

export default LoginModal;