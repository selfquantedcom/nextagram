import React from 'react';

const Loader = (props) => {
   return <div className={"col-12 mt-5"}><div className="spinner-border" role="status"></div></div>
}

export default Loader