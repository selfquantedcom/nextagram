import React from 'react';
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Button } from 'reactstrap';
import axios from 'axios';
import {storageSave, storageLoad} from "../Storage"

class LoginForm extends React.Component {
  state = {   
    password: "",
    email: "",
    invalidLogin: false
  }  

  handlePasswordInput = (event) => {
    this.setState({ password: event.target.value })
  }

  handleEmailInput = (event) => {
    this.setState({ email: event.target.value })
  }

  submitLoginForm = (event) => {    
    event.preventDefault();    
    axios({
      method: 'POST',
      url: 'https://insta.nextacademy.com/api/v1/login',
      data: {        
        email: this.state.email,
        password: this.state.password
      }
    })
    .then(response => {
      if (response.data.status == "success"){    
        storageSave('nextAcademyProfileImg', response.data.user.profile_picture)        
        storageSave('nextAcademyJWT', response.data.auth_token)
        this.props.toggle()        
      }
      
    })
    .catch(error => {
      this.setState({email: "", password: "", invalidLogin: true});
    })  
  }

  render(){
    return(
      <>
      <Form onSubmit={this.submitLoginForm}> 
        <FormGroup>          
          <Label for="email">Email</Label>     
          <Input type="text"
                 onChange={this.handleEmailInput}
                 value={this.state.email}                
                 />                     
          <Label for="password">Password</Label>
          <Input  type="password"
                  onChange={this.handlePasswordInput}
                  value={this.state.password} />     
        </FormGroup>
        <FormText>New user? <span className="toggle-forms-link" onClick={this.props.toggleForm}>Register here</span></FormText>
        <div className="row">
          <Button {...(this.state.email !== "" && this.state.password !== "" ? {enabled: true} : {disabled: true}) } type="submit" className="mr-auto ml-auto" color="success">Submit</Button>                    
        </div>
        <div className="row">
          {this.state.invalidLogin ? <FormText className="mr-auto ml-auto mt-2">Invalid username or password</FormText> : ""}
        </div>
      </Form> 
      </>
    )
  }
}

export default LoginForm