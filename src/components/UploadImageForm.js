import React from 'react';
import axios from 'axios';
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Button } from 'reactstrap';
import {storageSave, storageLoad} from "../Storage";

class UploadImageForm extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      imageFile: null,
      previewImage: null,
      message: "Upload image" 
    }
  }

  handleFile = (e) => { 
    this.setState({imageFile: e.target.files[0], previewImage: URL.createObjectURL(e.target.files[0])});    
  }

  submitFile = (e) => {         
    e.preventDefault();   
    const jwt = storageLoad("nextAcademyJWT");
    if(jwt !== "not found"){      
      const headers = {Authorization: `Bearer ${jwt}`};
      let formData = new FormData()
      formData.append('image', this.state.imageFile)      
      axios({
        method: 'POST',
        url: 'https://insta.nextacademy.com/api/v1/images/',
        headers: headers,
        data: formData
      })
      .then(response => {        
        this.setState({previewImage: null, message: "Upload image"})
      })
      .catch(error => {
        console.log(error);
      })  
    }
  }

  render(){
    const {previewImage, message} = this.state;
    return(
      <>
      <Form onSubmit={this.submitFile}>
        <FormGroup>
        <div className="card ml-auto mr-auto">
          {previewImage ? (
            <img src={previewImage} width="100%"/>
            ) : (
            <h3 className="text-center">
              {message ? message : "Live Preview"}
            </h3>
          )}               
          <Input
            type="file"
            name="image-file"
            multiple="false"            
            onChange={this.handleFile}            
          />          
        </div>
          <FormText color="muted">
            Make sure the image being uploaded is a supported format.
          </FormText>
        </FormGroup>
        <Button type="submit" className={"upload-img-btn"}>
          Upload
        </Button>
      </Form>
      </>
    )
  }
}

export default UploadImageForm;