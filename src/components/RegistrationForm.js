import React from 'react';
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Button } from 'reactstrap';
import axios from 'axios';
import {storageSave, storageLoad} from "../Storage";

class RegistrationForm extends React.Component {
  state = {
    username: "",
    email: "",
    password: "",
    passwordConfirmation: "",
    usernameValid: false,
    emailValid: false,
    passwordValid: false,
    matchingPassword: false

  }

  handleUserNameInput = (e) => {
    let x = { ...e };
    let delay = setTimeout(() => this.handleUsernameCheck(x), 300);
    this.setState({ username: e.target.value, delay })
  }  

  handleUsernameCheck = (e) => {
    const newUsername = e.target.value;    
    if (newUsername.length >= 6) {
      axios.get(
        `https://insta.nextacademy.com/api/v1/users/check_name?username=${newUsername}`
      ).then(response => {        
        if (response.data.valid) {
          this.setState({
            usernameValid: true
          });
        } else {
          this.setState({
            usernameValid: false
          });
        }
      });
    } else{
      this.setState({
        usernameValid: false
      });

    }
  }

  validateEmail = (e) => {     
    if(e){   
      let email = e.target.value.trim().toLowerCase();
      if (/^\S+@\S+\.\S+$/.test(email)) {
          this.setState({emailValid: true, email: email})        
      } else {
          this.setState({emailValid: false, email: email})
      }
    }
  }

  usernameValid = ()  =>{
    if(this.state.username.length > 0){
      if(this.state.usernameValid){
        return "Cool username"
      } else{
          return "Username already exists"     
      }
    } else{
        return "bla"
    }
  }

  handlePasswordInput = (e) => {    
    if (e.target.value.length > 8 && e.target.value.length < 50){
      this.setState({password: e.target.value, passwordValid: true})
    } else{
      this.setState({password: e.target.value, passwordValid: false})
    }

  }

  handlePasswordConfirmationInput = (e) => {
    if(this.state.password == e.target.value){
      this.setState({passwordConfirmation: e.target.value, matchingPassword: true});    
    } else{
      this.setState({passwordConfirmation: e.target.value, matchingPassword: false});    
    }
  }  

  submitRegistrationForm = (event) => {
    event.preventDefault();
    axios({
      method: 'POST',
      url: 'https://insta.nextacademy.com/api/v1/users/',
      data: {
        username: this.state.username,
        email: this.state.email,
        password: this.state.password
      }
    })
    .then(response => {
      if(response.data.status == "success")      
      storageSave('nextAcademyJWT', response.data.auth_token)
    })
    .catch(error => {
      console.error(error.response) // so that we know what went wrong if the request failed
    })  
  }

  render(){
     return(
     <>
     <Form onSubmit={this.submitRegistrationForm}> 
       <FormGroup >
         <Label for="username">Username</Label>
         <Input type="text" 
                name="username" 
                value={this.state.username}
                onChange={e => {
                   if (this.state.delay) {
                     clearTimeout(this.state.delay);
                   }
                   this.handleUserNameInput(e);                   
                   }}              
                   {...(this.state.username.length > 0 ? 
                     this.state.usernameValid ? {valid: true} : {invalid: true} : "")}
             />     
         <FormFeedback {...(this.state.username.length > 0 ? 
                     this.state.usernameValid ? {valid: true} : {invalid: true} : "")}>{this.state.usernameValid ? "User name is available" : "This username has been taken or has fewer than 6 chars"}</FormFeedback>     
        </FormGroup>
        
        <FormGroup >
           <Label for="email">Email</Label>     
           <Input type="email" 
                  name="email" 
                  value={this.state.email}
                  onChange={e => this.validateEmail(e)}              
                     {...(this.state.email.length > 0 ? 
                     this.state.emailValid ? {valid: true} : {invalid: true} : "")}                  
                  />     
           <FormFeedback{...(this.state.email.length > 0 ? 
                     this.state.emailValid ? {valid: true} : {invalid: true} : "")}>{this.state.emailValid ? "Thank you :)" : "Invalid email format"}</FormFeedback>
         </FormGroup>

         <FormGroup >
           <Label for="password">Password</Label>     
           <Input type="password"
                  onChange = {this.handlePasswordInput}                  
                  value={this.state.password}
                  {...(this.state.password.length > 0 ? 
                     this.state.passwordValid ? {valid: true} : {invalid: true} : "")}/>                              
           <FormFeedback {...(this.state.password.length > 0 ? 
                     this.state.passwordValid ? {valid: true} : {invalid: true} : "")}>{this.state.passwordValid ? "Cool password. Very original ;)" : "Password must be between 8 and 50 chars"}</FormFeedback>                  
           </FormGroup>

           <FormGroup>
           <Label for="passwordConfirmation">Password confirmation</Label>     
           <Input type="password" 
                  value={this.state.passwordConfirmation} 
                  onChange={e => this.handlePasswordConfirmationInput(e)} 
                  {...(this.state.passwordConfirmation.length > 0 ? 
                     this.state.matchingPassword ? {valid: true} : {invalid: true} : "")}                  
                  />     

           <FormFeedback {...(this.state.passwordConfirmation.length > 0 ? 
                     this.state.matchingPassword ? {valid: true} : {invalid: true} : "")}>{this.state.matchingPassword ? "Passwords are matching" : "Passwords are NOT matching"}</FormFeedback>                  
         </FormGroup>
       
       <FormText>Already registered? <span className="toggle-forms-link" onClick={this.props.toggleForm}>Login here</span></FormText>
       <div className="row">
          <Button type="submit" {...(this.state.usernameValid && this.state.emailValid && this.state.passwordValid && this.state.matchingPassword ? {enabled: true} : {disabled: true})} className="mr-auto ml-auto" color="success">Submit</Button>           
        </div>
     </Form>
     </>
     )   
  }
}

export default RegistrationForm