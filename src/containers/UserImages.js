import React from 'react';
import { Link } from 'react-router-dom';

const UserImages = ({id, profileImage, username}) => {
  
   return(
   <div className={"col-xs-12 col-sm-6 col-md-4 col-lg-3 mt-5"}>
     <div className={"card ml-auto mr-auto"}>
        <img className={"card-img-top"} src={profileImage} alt={username}></img>
        <div className={"card-body"}>
           <h4 className={"card-title"}>{username}</h4>                         
           <Link className={"btn btn-primary"} to={`/users/${id}`}>Show Images</Link>
        </div>
     </div>              
   </div>
   )
}

export default UserImages