import React from "react";
import Image from "react-graceful-image";
import Loader from "../components/Loader";
import { Link } from 'react-router-dom';
import axios from 'axios';

class UserProfilePage extends React.Component {  
  state = {
    userImages: [],
    loadingImages: true

  }

  componentDidMount(){      
      this.setState({loadingImages:true});    
      axios.get(`https://insta.nextacademy.com/api/v1/images?userId=${this.props.match.params.id}`)
      .then(result => {    
         this.setState({loadingImages:false, userImages: result.data});             
       })
       .catch(error => {      
         alert('ERROR: ', error);          
      })
  }

  render() {
    
    const { users, match: { params: { id}}} = this.props
    if (!users.length) return <div className={"container container-fluid"}> 
                                <div className={"row"}><Loader/></div>
                              </div>

    const currentUser = this.props.users.find(user => user.id == this.props.match.params.id)
    return (
       <>    
       <div className={"container container-fluid"}>   
         <div className={"col-sm-12 mt-5"}>
           <div className={"card ml-auto mr-auto"}>                                  
              <img className={"card-img-top"} src={currentUser.profileImage} alt={currentUser.username}></img>
              <div className={"card-body"}>
                 <h4 className={"card-title"}>{currentUser.username}</h4>   
                 <Link className={"btn btn-primary"} to={'/'}>Back to Home Page</Link>
              </div>
           </div>              
         </div>      

         <div className={"row mt-5 mb-5"}>
         {this.state.loadingImages ? <Loader/> : this.state.userImages.map(image => 
           <div className={"col-xs-12 col-sm-6 col-md-4 col-lg-3"}> <Image src={image} className={"img-fluid img-thumbnail"} width="250" height="250"/></div> )}
         </div>
      </div>
      </>
      )
  }
}

export default UserProfilePage