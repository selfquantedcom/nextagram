import React from 'react';
import UserImages from '../containers/UserImages';
import Image from "react-graceful-image";
import Loader from "../components/Loader";


class HomePage extends React.Component {
   state = {
    showUserImages: false,
    loadingImages: false,
    userImages: [],
    selectedUser: null    
  }

  homePage = () => {this.setState({showUserImages: false, loadingImages: false, selectedUser: null})}

  getUser = (id) =>{
    for (let i = 0; i < this.props.users.length; i++) { 
      if(this.props.users[i].id == id){return this.props.users[i]}     
    }
  }

  goToPreviousPage = (event) => {
    this.props.history.push(`/users/${event.target.id}`);
  }

   

  render() {
    return (    
      <div className={"container container-fluid mb-5"}>      
        <h1 className={"mt-5"}>Welcome to Nextagram!</h1>       
        <div className={"row"}>       
          {this.props.awaitingUsersResponse || this.state.loadingImages ? 
            <Loader/> : ""
          }            
           {this.props.users.map(user => <UserImages goToPreviousPage={this.goToPreviousPage} getUserImages={this.getUserImages} profileImage={user.profileImage} username={user.username} id={user.id}/>)}
        </div>
      </div>     
    )
  }
}

export default HomePage