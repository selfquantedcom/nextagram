import React from 'react';
import axios from 'axios';
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Button } from 'reactstrap';
import {storageSave, storageLoad} from "../Storage";
import {Link} from "react-router-dom";
import Loader from "../components/Loader";
import UploadImageForm from "../components/UploadImageForm";
import Image from "react-graceful-image";

class MyProfilePage extends React.Component {
  state = {      
    userLogedIn: false,
    loadingImages: true,
    userImages: []
  }    

  componentDidMount() {
    const jwt = storageLoad("nextAcademyJWT");
    if(jwt !== "not found"){
      this.setState({userLogedIn: true, loadingImages: true});
      const headers = {Authorization: `Bearer ${jwt}`};
      axios.get('https://insta.nextacademy.com/api/v1/images/me', {headers: headers})
     .then(result => {    
         console.log(result)
         this.setState({loadingImages: false, userImages: result.data});
       })
       .catch(error => {      
         console.log(error)
         this.setState({loadingImages: false});
      }) 
    } 
  }

  render(){   
    return(      
      <>
      {this.state.userLogedIn ? (    
      <div className="container container-fluid">
       <div className={"col-sm-12 mt-5"}>
           <div className={"card ml-auto mr-auto"}>                                  
              <img className={"card-img-top"} src={storageLoad("nextAcademyProfileImg")} ></img>
              <div className={"card-body"}>
                 <h4 className={"card-title"}></h4>   
                 <Link className={"btn btn-primary"} to={'/'}>Back to Home Page</Link>
              </div>
           </div>              
         </div>      

         <div className="row">
            <div className="mr-auto ml-auto mt-2">
               <UploadImageForm> </UploadImageForm>
            </div>
         </div>

         <div className={"row mt-5 mb-5"}>
         {this.state.loadingImages ? <Loader/> : this.state.userImages.map(image => 
           <div className={"col-xs-12 col-sm-6 col-md-4 col-lg-3"}> <Image src={image} className={"img-fluid img-thumbnail mt-3"} width="250" height="250"/></div> )}
         </div>
      </div>     
      ) :
      (
        <div className="container container-fluid">
        <div className="row">
            <div className="mr-auto ml-auto mt-2">
               <h1>Please login</h1>
            </div>
          </div>
      </div>
      )}      
      </>     
    )   
   }
}


export default MyProfilePage