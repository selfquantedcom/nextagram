import React from 'react';
import './App.css';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import 'bootstrap/dist/css/bootstrap.css';
import HomePage from './pages/HomePage';
import {Navbar, NavbarBrand, Button } from 'reactstrap';
import { Route, Link, Switch } from "react-router-dom";
import UserProfilePage from "./pages/UserProfilePage";
import MyProfilePage from "./pages/MyProfilePage";
import LoginModal from "./components/LoginModal";
import {storageSave, storageLoad, storageRemove} from "./Storage";
library.add(fab)

class App extends React.Component {
  state = {
    users: [],
    awaitingUsersResponse: true,
    userLogedIn: false,        
  }

  componentDidMount() {
    if(storageLoad("nextAcademyJWT") !== "not found"){
        this.setState({userLogedIn: true});
    }

    // performing a GET request    
    axios.get('https://insta.nextacademy.com/api/v1/users')
    .then(result => {         
      this.setState({users: result.data, awaitingUsersResponse: false});
    })
    .catch(error => {      
      alert('ERROR: ', error)
       this.setState({awaitingUsersResponse: false});
    })    
}
    userLogedIn = () => {
      if(storageLoad("nextAcademyJWT") !== "not found"){
        this.setState({userLogedIn: true});
      }
    }

    logout = () => {
      storageRemove("nextAcademyJWT")
      this.setState({userLogedIn: false})
    }    

  render(){
    return (
      <>
      <Navbar color="light" light expand="md">
         <NavbarBrand href="" className={"mr-auto ml-auto"}><Link className={"navbar-link"} to={'/'}><FontAwesomeIcon icon={['fab', 'instagram']} />  |  Nextagram</Link></NavbarBrand>     
         {this.state.userLogedIn ? (
           <>
           <Link className={"btn btn-primary mr-2"} to={'/profile'}>Profile Page</Link>
           <Button color="danger" onClick={this.logout}> Logout </Button> 
           </>)
           : 
           <LoginModal setProfilePicture={this.setProfilePicture} userLogedIn={this.userLogedIn} buttonLabel={"Login"}/>
         }            
      </Navbar>      
     
      <div>        
        <Switch>
          <Route exact path="/profile" component={MyProfilePage}/>
          <Route path="/users/:id" component={props => {return <UserProfilePage {...props} users={this.state.users} />}} />          
          <Route path="/" component={props => {return <HomePage {...props} awaitingUsersResponse={this.state.awaitingUsersResponse} users={this.state.users}/>}}          />           
        </Switch>
      </div>            
      </>     
    )
  }
}

export default App;

